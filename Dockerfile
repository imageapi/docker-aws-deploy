FROM node:10.15.0

SHELL ["/bin/bash", "-c"]

RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
  unzip awscli-bundle.zip && \
  ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws && \
  rm -rf ./awscli-bundle awscli-bundle.zip

RUN npm install --global yarn

RUN aws --version && \
  node --version && \
  yarn --version

ADD assume-role.sh /root/assume-role.sh
RUN echo "source /root/assume-role.sh" >> ~/.bashrc
